package com.reneboeser.springboot.model;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class Task {

    @Id
    @GeneratedValue
    private Long id;

    private Date createdOn = new Date();

    private String title;

    public Task(String title) {
        this.title = title;
    }
}
