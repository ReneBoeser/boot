package com.reneboeser.springboot.contrl;

import com.reneboeser.springboot.model.Task;
import com.reneboeser.springboot.repo.TaskRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskRepo taskRepo;

    @GetMapping("/")
    public Iterable<Task> getAll() {
        return taskRepo.findAll();
    }

    @GetMapping("/{id}")
    public Task getTask(@PathVariable Long id) {
        return taskRepo.findById(id).get();
    }
}
