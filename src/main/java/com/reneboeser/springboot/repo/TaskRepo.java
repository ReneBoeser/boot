package com.reneboeser.springboot.repo;

import com.reneboeser.springboot.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TaskRepo extends CrudRepository<Task, Long> {

    Optional<Task> findById(long id);

    Task save(Task task);

    Iterable<Task> findAll();
}