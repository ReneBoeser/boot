package com.reneboeser.springboot.repo;

import com.reneboeser.springboot.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class TestDataInsertListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    TaskRepo taskRepo;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        taskRepo.save(new Task("buy milk"));
        taskRepo.save(new Task("feed the cat"));
    }
}