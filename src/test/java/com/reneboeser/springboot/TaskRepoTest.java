package com.reneboeser.springboot;

import com.reneboeser.springboot.model.Task;
import com.reneboeser.springboot.repo.TaskRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskRepoTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    TaskRepo taskRepo;

    @Test
    public void shouldSaveNewTask() {
        String title = "milk the cow";
        Task task = new Task(title);

        taskRepo.save(task);

        Optional<Task> persistedTask = taskRepo.findById(1);
        assertThat(persistedTask, is(notNullValue()));
        assertThat(persistedTask.get().getTitle(), is(title));
    }

    @Test
    public void shouldFindTaskById() {
        String title = "ring the bell";
        Task task = new Task(title);
        entityManager.persist(task);

        Optional<Task> foundTask = taskRepo.findById(2);

        assertThat(foundTask.get().getTitle(), is(title));
    }
}
